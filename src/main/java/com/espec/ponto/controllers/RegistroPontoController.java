package com.espec.ponto.controllers;

import com.espec.ponto.models.RegistroPonto;
import com.espec.ponto.models.Usuario;
import com.espec.ponto.services.RegistroPontoService;
import com.espec.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/ponto")
public class RegistroPontoController {
    @Autowired
    RegistroPontoService registroPontoService;

    @PostMapping
    public ResponseEntity<RegistroPonto> marcarPonto(@RequestBody RegistroPonto registroPonto){
        RegistroPonto rPontoObj;
        try{
            rPontoObj = registroPontoService.registrarPonto(registroPonto);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(201).body(rPontoObj);
    }

    @GetMapping("/{usuario}")
    public ResponseEntity<String> buscarPorId(@PathVariable Integer usuario) {
        try {
            String retorno = registroPontoService.listagemPonto(usuario);
            return ResponseEntity.status(200).body(retorno);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<String> buscarTudo() {
        try {
            String retorno = registroPontoService.listagemPonto();
            return ResponseEntity.status(200).body(retorno);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
