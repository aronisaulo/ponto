package com.espec.ponto.repository;

import com.espec.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>  {
}
