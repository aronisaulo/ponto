package com.espec.ponto.repository;

import com.espec.ponto.models.RegistroPonto;
import org.springframework.data.repository.CrudRepository;

public interface RegistroPontoRepository extends CrudRepository<RegistroPonto, Integer> {
}
