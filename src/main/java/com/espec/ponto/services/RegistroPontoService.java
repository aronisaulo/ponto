package com.espec.ponto.services;

import com.espec.ponto.enums.TipoBatida;
import com.espec.ponto.models.RegistroPonto;
import com.espec.ponto.models.Usuario;
import com.espec.ponto.repository.RegistroPontoRepository;
import com.espec.ponto.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RegistroPontoService {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    RegistroPontoRepository registroPontoRepository;

    public RegistroPonto registrarPonto(RegistroPonto registroPonto) {
        registroPonto.setDataHora(new Date());
        return registroPontoRepository.save(registroPonto);

    }

    public String listagemPonto(int usuario) {

        List<String> regitroRetorno = new ArrayList<String>();
        Optional<Usuario> optUsuario = usuarioRepository.findById(usuario);
        if (optUsuario.isPresent()) {
            regitroRetorno.add("Nome:" + optUsuario.get().getNomeCompleto());
            regitroRetorno.add("Horas Trabalhadas :" + recuperaTempoTrabalhado(usuario));
        }
        return regitroRetorno.toString();
    }


    public String listagemPonto() {

        List<String> regitroRetorno = new ArrayList<String>();
        Iterable<Usuario> iteUsuario = usuarioRepository.findAll();
        for (Usuario usu : iteUsuario) {
            regitroRetorno.add("Nome:" + usu.getNomeCompleto());
            regitroRetorno.add("Horas Trabalhadas :" + recuperaTempoTrabalhado(usu.getId()));
        }
        return regitroRetorno.toString();
    }

    private String recuperaTempoTrabalhado(int usuario) {
        Date dtEntrada = new Date() ;
        Date dtSaida = new Date();

        Iterable<RegistroPonto> lista = registroPontoRepository.findAll();
        for (RegistroPonto registroPonto : lista) {
            if (registroPonto.getUsuario() == usuario) {
                if (registroPonto.getTipoBatida().equals(TipoBatida.ENTRADA)) {
                    dtEntrada = registroPonto.getDataHora();
                } else if (registroPonto.getTipoBatida().equals(TipoBatida.SAIDA)) {
                    dtSaida = registroPonto.getDataHora();
                    continue;
                }

            }
        }
        return  horasTrabalhadas(dtEntrada, dtSaida);
    }

    private String horasTrabalhadas(Date dtEntrada, Date dtSaida) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
        sdf1.format(dtEntrada);
        sdf2.format(dtSaida);
        double ms= sdf2.getCalendar().getTimeInMillis() - sdf1.getCalendar().getTimeInMillis();
        long segundos =  (long) ms / 1000;
        long minutos = segundos / 60;
        segundos = segundos % 60;
        long horas = minutos / 60;
        minutos = minutos % 60;
        return String.format ("%02d:%02d:%02d", horas, minutos, segundos);
    }
}
