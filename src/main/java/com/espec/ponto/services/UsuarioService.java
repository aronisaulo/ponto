package com.espec.ponto.services;

import com.espec.ponto.models.Usuario;
import com.espec.ponto.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService  {
    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        usuarioRepository.save(usuario);
        return usuario;
    }

    public Optional consultaUsuario(int id){
        return usuarioRepository.findById(id);
    }

    public Iterable<Usuario> listaUsuario(){
        return usuarioRepository.findAll();
    }



}
