package com.espec.ponto.models;

import com.espec.ponto.enums.TipoBatida;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import java.util.Date;
@Entity
public class RegistroPonto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int usuario;
    private Date dataHora;
    @NotNull(message="Informar a Tipo de entrada")
    private TipoBatida tipoBatida;

    public RegistroPonto(int id, int usuario, Date dataHora, TipoBatida tipoBatida) {
        this.id = id;
        this.usuario = usuario;
        this.dataHora = dataHora;
        this.tipoBatida = tipoBatida;
    }

    public RegistroPonto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public TipoBatida getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatida tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
