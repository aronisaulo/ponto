package com.espec.ponto.controllers;

import com.espec.ponto.models.Usuario;
import com.espec.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.*;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UsuarioService usuarioService;

    ObjectMapper oMapper = new ObjectMapper();
    Usuario usuario;

    @BeforeEach
    public void iniciar() {
        Calendar calendar = new GregorianCalendar();
        calendar = new GregorianCalendar(2020, Calendar.MAY + 1, 25);
        usuario = new Usuario( 1,"Joao Pessoa", "999999999","joao@joao.com.br", calendar.getTime());

     }

    @Test
    public void cadastrarUsuarioTest() throws Exception {
        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class))).thenReturn(usuario);
        String json = oMapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
               .andExpect(MockMvcResultMatchers.jsonPath("$.nomeCompleto", CoreMatchers.equalTo("Joao Pessoa")));
    }

    @Test
    public void cadastroUsuarioErroTest() throws Exception {

        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class))).thenThrow(new RuntimeException());
        String json = oMapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void buscarUsuarioTest() throws Exception {
        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario, usuario);
        Mockito.when(usuarioService.listaUsuario()).thenReturn(usuarioIterable);

        String json = oMapper.writeValueAsString(usuarioIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
    @Test
    public void buscarUsuarioInexistenteTest() throws Exception {
        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario, usuario);
        Mockito.when(usuarioService.listaUsuario()).thenThrow(new RuntimeException());

        String json = oMapper.writeValueAsString(usuarioIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void buscarUsuarioIDTest() throws Exception {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioService.consultaUsuario(Mockito.anyInt())).thenReturn(usuarioOptional);

        String json = oMapper.writeValueAsString(usuarioOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void buscarUsuarioIDInexistenteTest() throws Exception {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioService.consultaUsuario(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = oMapper.writeValueAsString(usuarioOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }
    
    
    
    
}
