package com.espec.ponto.controllers;

import com.espec.ponto.enums.TipoBatida;
import com.espec.ponto.models.RegistroPonto;
import com.espec.ponto.models.Usuario;
import com.espec.ponto.services.RegistroPontoService;
import com.espec.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

@WebMvcTest(RegistroPontoController.class)
public class RegistroPontoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UsuarioService usuarioService;

    @MockBean
    RegistroPontoService registroPontoService;

    ObjectMapper oMapper = new ObjectMapper();
    Usuario usuario;
    RegistroPonto registroPontoE, registroPontoS;

    @BeforeEach
    public void iniciar() {
        Calendar calendar = new GregorianCalendar();
        calendar = new GregorianCalendar(2020, Calendar.MAY + 1, 25);
        usuario = new Usuario(1, "Joao Pessoa", "999999999", "joao@joao.com.br", calendar.getTime());
        registroPontoE = new RegistroPonto(1, 1, new Date(), TipoBatida.ENTRADA);
        registroPontoS = new RegistroPonto(2, 1, new Date(), TipoBatida.SAIDA);

    }

    @Test
    public void marcarPontoTest() throws Exception {
        Mockito.when(registroPontoService.registrarPonto(Mockito.any(RegistroPonto.class))).thenReturn(registroPontoE);
        String json = oMapper.writeValueAsString(registroPontoE);

        mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void marcarPontoErroTest() throws Exception {

        Mockito.when(registroPontoService.registrarPonto(Mockito.any(RegistroPonto.class))).thenThrow(new RuntimeException());
        String json = oMapper.writeValueAsString(registroPontoE);

        mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void listaPontoTest() throws Exception {
       String retorno = "";
        Mockito.when(registroPontoService.listagemPonto()).thenReturn(retorno);

        String json = oMapper.writeValueAsString(retorno);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void listaPontoInexistenteTest() throws Exception {

        Mockito.when(registroPontoService.listagemPonto()).thenThrow(new RuntimeException());

        String json = oMapper.writeValueAsString("");

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void listaPontoIDTest() throws Exception {
        String retorno = "nomeCompleto: Joao Pessoa";
        Mockito.when(registroPontoService.listagemPonto(Mockito.anyInt())).thenReturn(retorno);

        String json = oMapper.writeValueAsString(retorno);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void listaPontoIDInexistenteTest() throws Exception {
        String retorno = "nomeCompleto: Joao Pessoa";
        Mockito.when(registroPontoService.listagemPonto(Mockito.anyInt())).thenThrow(new RuntimeException());

        String json = oMapper.writeValueAsString(retorno);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }


}