package com.espec.ponto.services;

import com.espec.ponto.enums.TipoBatida;
import com.espec.ponto.models.RegistroPonto;
import com.espec.ponto.models.Usuario;
import com.espec.ponto.repository.RegistroPontoRepository;
import com.espec.ponto.repository.UsuarioRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

@SpringBootTest
public class RegistroPontoServiceTest {
    @Autowired
    UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private RegistroPontoRepository registroPontoRepository;

    @Autowired
    RegistroPontoService registroPontoService;

    ObjectMapper oMapper = new ObjectMapper();
    Usuario usuario;
    RegistroPonto registroPontoE, registroPontoS;

    @BeforeEach
    public void iniciar() {
        Calendar calendar = new GregorianCalendar();
        calendar = new GregorianCalendar(2020, Calendar.MAY + 1, 25);
        usuario = new Usuario(1, "Joao Pessoa", "999999999", "joao@joao.com.br", calendar.getTime());
        registroPontoE = new RegistroPonto(1, 1, new Date(), TipoBatida.ENTRADA);
        registroPontoS = new RegistroPonto(2, 1, new Date(), TipoBatida.SAIDA);

    }

    @Test
    public void deveRegistrarPontoEntrada() {
        Mockito.when(registroPontoRepository.save(Mockito.any(RegistroPonto.class))).thenReturn(registroPontoE);
        RegistroPonto registroPontoRetorno = registroPontoService.registrarPonto(registroPontoE);
        Assertions.assertEquals(registroPontoE.getId(), registroPontoRetorno.getId());
    }

    @Test
    public void deveListagemRegistrarPontoEntrada() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Iterable<RegistroPonto> registroPontoIterable = Arrays.asList(registroPontoE,registroPontoS);
        Mockito.when(registroPontoRepository.findAll()).thenReturn(registroPontoIterable);
        String registroPontoRetorno = registroPontoService.listagemPonto(usuario.getId());

        Assertions.assertNotEquals("", registroPontoRetorno);
    }
    @Test
    public void deveListagemRegistrarPontoGeral() {
        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioIterable);
        Iterable<RegistroPonto> registroPontoIterable = Arrays.asList(registroPontoE,registroPontoS);
        Mockito.when(registroPontoRepository.findAll()).thenReturn(registroPontoIterable);
        String registroPontoRetorno = registroPontoService.listagemPonto();

        Assertions.assertNotEquals("", registroPontoRetorno);
    }
}
