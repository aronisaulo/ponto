package com.espec.ponto.services;

import com.espec.ponto.models.Usuario;
import com.espec.ponto.repository.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {
    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void InicializarTestes() {
        Calendar calendar = new GregorianCalendar();
        calendar = new GregorianCalendar(2020, Calendar.MAY + 1, 25);
        usuario = new Usuario( 1,"Joao Pessoa", "999999999","joao@joao.com.br", calendar.getTime());
    }

    @Test
    public void deveSalvar() {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioRetornado = usuarioService.salvarUsuario(usuario);

        Assertions.assertEquals(usuario.getId(), usuarioRetornado.getId());
        Assertions.assertEquals(usuario.getEmail(), usuarioRetornado.getEmail());
        Assertions.assertEquals(usuario.getNomeCompleto(), usuarioRetornado.getNomeCompleto());
        Assertions.assertEquals(usuario.getDataCadastro(), usuarioRetornado.getDataCadastro());

    }

    @Test
    public void deveBuscarId() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        int id = 1;
        Optional usuarioOptional = usuarioService.consultaUsuario(id);
        Assertions.assertEquals(usuario, usuarioOptional.get());
        Assertions.assertEquals(usuario.getNomeCompleto(),((Usuario) usuarioOptional.get()).getNomeCompleto());
    }
    @Test
    public void deveBuscarTodosUsuarios() {
        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioIterable);
        int id = 1;
        Iterable<Usuario> iterableResultado = usuarioService.listaUsuario();
        Assertions.assertEquals(usuarioIterable, iterableResultado);
    }





}
