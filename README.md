# Registro de Ponto

Projeto tem que como banco de dados MySql

#Inicializar o serviço Mysql
sudo systemctl start mariadb.service

#Acessando MySQL

 mysql -u root -p
<enter>

#Criar Banco de Dados 
CREATE DATABASE PONTO;

CREATE USER 'rh'@'localhost' IDENTIFIED BY '123';

GRANT ALL ON PONTO.* TO 'rh'@'localhost' IDENTIFIED BY '123';

# APIS 
#cadastro de usuário

POST - http://localhost:8080/usuarios
JSON 
{
	  "nomeCompleto":"EDSON CARNEIRO",
	  "cpf":"12345678901" ,
	  "email":"eneas@prona.gov",
	  "dataCadastro":"2020-07-20"
}

#consulta de usuário especifico
GET - http://localhost:8080/usuarios/1

consulta de todos usuários
GET - http://localhost:8080/usuarios

# Registro de Ponto de Entrada
POST - http://localhost:8080/ponto

JSON
{
    "usuario":1,
    "tipoBatida":"ENTRADA"
}
RETORNO JSON
{
    "id": 1,
    "usuario": 1,
    "dataHora": "2020-07-24T01:28:44.083+0000",
    "tipoBatida": "ENTRADA"
}

# Registro de Ponto de Saida
POST - http://localhost:8080/ponto

JSON
{
    "usuario":1,
    "tipoBatida":"SAIDA"
}
RETORNO JSON
{
    "id": 2,
    "usuario": 1,
    "dataHora": "2020-07-24T01:33:44.083+0000",
    "tipoBatida": "SAIDA"
}

# LISTAGEM ESPECIFICA
GET - http://localhost:8080/ponto/2

RETORNO [Nome:EDSON CARNEIRO, Horas Trabalhadas :00:00:52]

# LISTAGEM TODOS
GET - http://localhost:8080/ponto

RETORNO [Nome:ENEAS CARNEIRO, Horas Trabalhadas :00:00:58, Nome:EDSON CARNEIRO, Horas Trabalhadas :00:00:52]










